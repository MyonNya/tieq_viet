use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn tieq_viet(text: &str) -> String {
    let mut result = String::with_capacity(text.len() / 2);
    let mut chars = text.chars().peekable();

    macro_rules! alphabet_shift {
        ($ch:expr, $op:tt $step:expr) => {
            ($ch as u8 $op $step) as char
        }
    }

    while let Some(ch) = chars.next() {
        let data = match ch {
            c @ 'c' | c @ 'C' => match chars.peek() {
                Some('h') | Some('H') => {
                    chars.next();
                    c // ch => c
                }

                _ => alphabet_shift!(c, +8), // c => k
            },

            t @ 't' | t @ 'T' => match chars.peek() {
                Some('r') | Some('R') => {
                    chars.next();
                    alphabet_shift!(t, -17) // tr => c
                }
                Some('h') | Some('H') => {
                    chars.next();
                    alphabet_shift!(t, +3) // th => w
                }
                _ => t,
            },

            g @ 'g' | g @ 'G' => match chars.peek() {
                Some('h') | Some('H') => {
                    chars.next();
                    g
                }
                Some('i') | Some('I') => {
                    chars.next();
                    alphabet_shift!(g, +19) // gi => z
                }
                Some('ì') | Some('ỉ') | Some('ĩ') | Some('ị') => {
                    alphabet_shift!(g, +19) // gì => zì
                }
                _ => g,
            },

            p @ 'p' | p @ 'P' => match chars.peek() {
                Some('h') | Some('H') => {
                    chars.next();
                    alphabet_shift!(p, -10) // ph => f
                }
                _ => p,
            },

            n @ 'n' | n @ 'N' => match chars.peek() {
                Some('h') | Some('H') => {
                    chars.next();
                    result.push(n);
                    '\''
                }
                Some('g') | Some('G') => {
                    chars.next();
                    if matches!(chars.peek(), Some('h') | Some('H')) {
                        chars.next();
                    }
                    alphabet_shift!(n, +3) // ng | ngh => q
                }
                _ => n,
            },

            k @ 'k' | k @ 'K' => match chars.peek() {
                Some('h') | Some('H') => {
                    chars.next();
                    alphabet_shift!(k, +13) // kh => x
                }
                _ => k,
            },

            q @ 'q' | q @ 'Q' => (q as u8 - 6) as char, // k
            'Đ' => 'D',
            'đ' => 'd',
            'd' | 'r' => 'z',
            'D' | 'R' => 'Z',

            v => v,
        };

        result.push(data);
    }

    result
}
