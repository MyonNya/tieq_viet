# Tieq Viet WebAssembly

## Yêu cầu cơ bản

- Cái máy tính
- Một cái trình duyệt web *hiện đại*
- [Trình biên dịch Rust](https://www.rust-lang.org/tools/install)
- [wasm-pack](https://rustwasm.github.io/wasm-pack/installer/)
<br>
Vì cái module này được việt bằng [Ngôn ngữ Rust](https://www.rust-lang.org/), nên bạn phải cài đặt nó trước<br>
Trên linux thì cứ đơn giản bằng câu lệnh

```sh
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

Và sau đó cài đặt wasm-pack, nó sẽ **rất** đơn giản nếu như...
```sh
curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh
```
*Note: nếu có yêu cầu quyên admin, thì chỉ cần thêm `sudo` vào phía trước `sh`*

## Khởi tạo
Sao chép cái mã nguồn này
```sh
git clone https://gitlab.com/tmokenc/tieq_viet.git
cd tieq_viet
```

Bây giờ bạn đã sẵn sàng sử dụng nó, chỉ bằng cách
```sh
wasm-pack build tieq_viet --target web
```

## Sử dụng
Tới thời điểm bây giờ thì bạn chỉ cần ném `main.html` vào trình duyệt và chạy thôi
### Chạy một web server
Nếu trong trường hợp bạn bị cái lỗi `Cross-Origin Request Blocked` <br>
Bạn có thể tắt nó trong trình duyệt (phương pháp thì tự đi mà google) hoặc chạy nó trên server bằng cách chạy dòng lệnh này
```sh
cargo run
```
Bây giờ thì bạn có thể vào url `127.0.0.1:8080` và thưởng thức<br><br>
*Web server sẽ tự động tạo wasm module cho nên dòng lệnh `wasm-pack` trên kia là không cần thiết*<br>
*Thêm `--release` để kích hoạt tối ưu cho web server, nhưng nó sẽ tăng thời gian biên dịch lên rất nhiều*<br>
*Thêm `IP` vào biến môi trường để sử dụng IP tuỳ chọn cho server, vd `IP=0.0.0.0:80`*

<hr>
Nếu có bị lỗi gì, thì thoải mái gọi cho `tmokenc#2067` trên Discord <br>
tmokenc @ 2020
