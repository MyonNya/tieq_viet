# Tieq Viet WebAssembly

## Basic requirement

- A computer
- A *modern* web browser
- [Rust compiler](https://www.rust-lang.org/tools/install)
- [wasm-pack](https://rustwasm.github.io/wasm-pack/installer/)
<br>
Since this module is written in the [Rust programming language](https://www.rust-lang.org/), you will have to install it first<br>
On linux system, it will be as easy as

```sh
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

Then install the wasm-pack, it will simplify our work **a lot**
```sh
curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh
```
*Note: if any of these steps requires root permission, just add `sudo` before the `sh`*

## Build it
Clone this repo
```sh
git clone https://gitlab.com/tmokenc/tieq_viet.git
cd tieq_viet
```

Now you are ready to build it, just simply run
```sh
wasm-pack build tieq_viet --target web
```

## Use it
At this point, you can just throw the `main.html` into the browser and there you go
### Run a web server
In case that you have run into the `Cross-Origin Request Blocked` error<br>
You can either disable that check in the browser (google it) or run it through a web server by simply run this command
```sh
cargo run
```
Now you can go to the url `127.0.0.1:8080` and enjoy<br><br>
*The web server will automatically build the wasm module so that `wasm-pack` command above is not necessary*<br>
*Add `--release` to enable optimization for the server, but it increase the compile time a lot*<br>
*Add `IP` to the environment variable to use custom IP for the server, eg `IP=0.0.0.0:80`*

<hr>
If there is any problem with this, feel free to contact me on Discord `tmokenc#2067` <br>
tmokenc @ 2020
